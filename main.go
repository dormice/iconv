package main

import (
	"flag"
	"fmt"
	"io/ioutil"

	"golang.org/x/text/encoding/charmap"
)

func main() {
	flag.Parse()

	for _, filename := range flag.Args() {
		fmt.Printf(" %s ... ", filename)

		content, err := ioutil.ReadFile(filename)
		if err != nil {
			fmt.Printf("error reading: %s\n", err)
			continue
		}

		content, err = charmap.Windows1251.NewDecoder().Bytes(content)
		if err != nil {
			fmt.Printf("error convert: %s\n", err)
			continue
		}

		err = ioutil.WriteFile(filename, content, 0644)
		if err != nil {
			fmt.Printf("error writing: %s\n", err)
		}

		fmt.Println("ok")
	}
}
